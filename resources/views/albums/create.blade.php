@extends('layouts.public')

@section('headerscripts')

@stop

@section('title', 'Add New Album')

@section('content')
<div class="row title-line">
 <div class="col-sm-8 col-md-6 col-md-offset-2">
  <h1>add new album</h1>
 </div>
</div>
<div class="row data">
<div class="col-md-8 col-md-offset-2">
<form class="form" action="/albums/" method="post">
 {{ csrf_field() }}
 <div class="form-group">
  <label for="band_id">band</label>
  <select class="form-control" name="band_id">
  @foreach($bands as $band)
  <option value="{{ $band->id }}" {{ $band->id === (int)$band_id ? 'selected' : '' }}>{{ $band->name }}</option>
  @endforeach
  </select>
 </div>
 <div class="form-group">
  <label for="name">album name:</label>
  <input class="form-control" type="text" name="name" value="">
 </div>
 <div class="form-group">
  <label for="recorded_date">recorded date:</label>
  <input class="form-control" type="text" name="recorded_date" value="">
 </div>
 <div class="form-group">
  <label for="release_date">release date:</label>
  <input class="form-control" type="text" name="release_date" value="">
 </div>
 <div class="form-group">
  <label for="number_of_tracks">number of tracks:</label>
  <input class="form-control" type="integer" name="number_of_tracks" value="">
 </div>
 <div class="form-group">
  <label for="producer">producer:</label>
  <input class="form-control" type="text" name="producer" value="">
 </div>
 <div class="form-group">
  <label for="genre">genre:</label>
  <input class="form-control" type="text" name="genre" value="">
 </div>

 <a class="btn btn-danger" href="{{ URL::previous() }}">back</a>
 <button class="btn btn-success" type="submit" name="button">save</button>
</form>
</div>
</div>

@stop