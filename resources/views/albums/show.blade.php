@extends('layouts.public')

@section('title', htmlspecialchars($album->name) . ' by ' .htmlspecialchars($album->band->name))

@section('content')

<div class="row title-line">
 <div class="col-sm-8 col-sm-offset-2">
  <h1>{{ $album->name }}</h1>
  <h2>by <a href="/bands/{{ $album->band->id }}">{{ $album->band->name }}</a></h2>
</div>
</div>
<div class="row data">
 <div class="col-sm-8 col-sm-offset-2">
  <div class="row">
   <div class="col-sm-12">
    <dl class="dl-horizontal well">
     <dt>name:</dt>
     <dd>{{ $album->name }}</dd>
     <dt>recorded date:</dt>
     <dd>{{ $album->recorded_date }}</dd>
     <dt>release date:</dt>
     <dd>{{ $album->release_date }}</dd>
     <dt>number of tracks:</dt>
     <dd>{{ $album->number_of_tracks }}</dd>
     <dt>label:</dt>
     <dd>{{ $album->label }}</dd>
     <dt>genre:</dt>
     <dd>{{ $album->genre }}</dd>
    </dl>
   </div>
  </div>
  <div class="row">
   <div class="col-sm-3">
    <a class="btn btn-info" href="{{ URL::previous() }}">back</a>
   </div>
   <div class="col-sm-3 col-sm-offset-1">
    <a class="btn btn-warning" href="/albums/{{ $album->id }}/edit">edit</a>
   </div>
   <div class="col-sm-3 col-sm-offset-1">
    <form class="" action="{{ route('albums.destroy', ['id' => $album->id]) }}" method="post">
      {{ method_field('DELETE') }}
      {{ csrf_field() }}
      <button class="btn btn-danger" type="submit" name="button">delete</button>
    </form>
   </div>
   </div>
  </div>
  </div>
 </div>
 @stop