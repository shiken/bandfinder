<header id="header" class="container-fluid">
  <div id="app-title" class="text-center">
   <span>band finder</span>
   <a class="btn btn-primary btn-header" href="/">view all bands</a>
   <a class="btn btn-primary btn-header" href="/albums">view all albums</a>
  </div>
</header>