<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>@yield('title') | bandfinder</title>

<!-- stylesheets -->
@include('partials._stylesheets')

<!-- header scripts -->
@include('partials._headerscripts')