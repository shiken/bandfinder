<?php

use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('albums')->insert([
      	'band_id' => 1,
      	'name'	  => 'Bloodgood',
      	'recorded_date' => '1986',
      	'release_date' => '1986',
      	'number_of_tracks' => 10,
      	'label' => 'frontline records',
      	'producer' => 'darryl mansfield',
      	'genre' => 'heavy metal',
      	'created_at' => date('Y-m-d H:i:s'),
      	'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 2,
        'name'    => 'Sounding the seventh trumpet',
        'recorded_date' => '2000',
        'release_date'  => '2001',
        'number_of_tracks'  => 13,
        'label' => 'goodlife recordings',
        'producer' => 'donnell cameroon',
        'genre' => 'metalcore',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Kasou musou shi',
        'recorded_date' => '2004',
        'release_date'  => '2005',
        'number_of_tracks'  => 7,
        'label' => 'king records & ps company',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Gion Shouja no Kane ga Naru',
        'recorded_date' => '2004',
        'release_date'  => '2004',
        'number_of_tracks'  => 5,
        'label' => 'king records & ps company',
        'producer' => 'alice nine',
        'genre' => 'punk rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Alice in wonderland',
        'recorded_date' => '2005',
        'release_date'  => '2005',
        'number_of_tracks'  => 5,
        'label' => 'king records & ps company',
        'producer' => 'alice nine',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Zekkeishoku',
        'recorded_date' => '2005',
        'release_date'  => '2006',
        'number_of_tracks'  => 12,
        'label' => 'king records & ps company',
        'producer' => 'alice nine',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Alpha',
        'recorded_date' => '2006',
        'release_date'  => '2007',
        'number_of_tracks'  => 14,
        'label' => 'king records & ps company',
        'producer' => 'tomomi ozaki (ps company) & koji shimana (king records)',
        'genre' => 'pop rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Discotheque play like "A" rainbows',
        'recorded_date' => '2008',
        'release_date'  => '2008',
        'number_of_tracks'  => 20,
        'label' => 'ps music',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Vandalize',
        'recorded_date' => '2008',
        'release_date'  => '2009',
        'number_of_tracks'  => 11,
        'label' => 'king records & ps company',
        'producer' => 'alice nine',
        'genre' => 'hard rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Gemini',
        'recorded_date' => '2010',
        'release_date'  => '2011',
        'number_of_tracks'  => 13,
        'label' => 'japan records',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => '9',
        'recorded_date' => '2012',
        'release_date'  => '2012',
        'number_of_tracks'  => 11,
        'label' => 'japan records',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Supernova',
        'recorded_date' => '2013',
        'release_date'  => '2014',
        'number_of_tracks'  => 12,
        'label' => 'universal music llc',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'Light and darkness',
        'recorded_date' => '2015',
        'release_date'  => '2016',
        'number_of_tracks'  => 7,
        'label' => 'nine head records',
        'producer' => 'alice nine',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 3,
        'name'    => 'ideal',
        'recorded_date' => '2016',
        'release_date'  => '2017',
        'number_of_tracks'  => 10,
        'label' => 'nine head records',
        'producer' => 'a9',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'zeitakubyō 「ゼイタクビョウ」',
        'recorded_date' => '2007',
        'release_date'  => '2007',
        'number_of_tracks'  => 11,
        'label' => 'a-sketch',
        'producer' => 'one ok rock',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'beam of light',
        'recorded_date' => '2007',
        'release_date'  => '2008',
        'number_of_tracks'  => 4,
        'label' => 'aer-born',
        'producer' => 'one ok rock',
        'genre' => 'post-hardcore',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'Kanjō Effect (感情エフェクト)',
        'recorded_date' => '2008',
        'release_date'  => '2008',
        'number_of_tracks'  => 8,
        'label' => 'aer-born',
        'producer' => 'one ok rock',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'Niche Syndrome',
        'recorded_date' => '2010',
        'release_date'  => '2010',
        'number_of_tracks'  => 4,
        'label' => 'aer-born',
        'producer' => 'one ok rock',
        'genre' => 'power pop',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'Zankyo Reference',
        'recorded_date' => '2011',
        'release_date'  => '2011',
        'number_of_tracks'  => 11,
        'label' => 'aer-born',
        'producer' => 'one ok rock',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'Jinsei × Boku',
        'recorded_date' => '2013',
        'release_date'  => '2013',
        'number_of_tracks'  => 13,
        'label' => 'aer-born',
        'producer' => 'one ok rock',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => '35xxxv',
        'recorded_date' => '2015',
        'release_date'  => '2015',
        'number_of_tracks'  => 13,
        'label' => 'aer-born',
        'producer' => 'john feldmann',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 6,
        'name'    => 'Ambitions',
        'recorded_date' => '2017',
        'release_date'  => '2017',
        'number_of_tracks'  => 14,
        'label' => 'aer-born',
        'producer' => 'colin brittain, john feldmann, mike shinoda',
        'genre' => 'rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Timeless',
        'recorded_date' => '2005',
        'release_date'  => '2006',
        'number_of_tracks'  => 13,
        'label' => 'gr8! records',
        'producer' => 'uverworld, chieko nakayama',
        'genre' => 'industrial rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Bugright',
        'recorded_date' => '2007',
        'release_date'  => '2007',
        'number_of_tracks'  => 13,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rap rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Proglution',
        'recorded_date' => '2008',
        'release_date'  => '2008',
        'number_of_tracks'  => 11,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Awakeve',
        'recorded_date' => '2009',
        'release_date'  => '2009',
        'number_of_tracks'  => 6,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Neo Sound Best',
        'recorded_date' => '2009',
        'release_date'  => '2009',
        'number_of_tracks'  => 7,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'alternative rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Last',
        'recorded_date' => '2010',
        'release_date'  => '2010',
        'number_of_tracks'  => 7,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Life 6 Sense',
        'recorded_date' => '2011',
        'release_date'  => '2011',
        'number_of_tracks'  => 4,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'The one',
        'recorded_date' => '2012',
        'release_date'  => '2012',
        'number_of_tracks'  => 9,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 9,
        'name'    => 'Ø Choir',
        'recorded_date' => '2014',
        'release_date'  => '2014',
        'number_of_tracks'  => 14,
        'label' => 'gr8! records',
        'producer' => 'uverworld',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'A.S.A Crew',
        'recorded_date' => '1999',
        'release_date'  => '1999',
        'number_of_tracks'  => 11,
        'label' => 'sky records',
        'producer' => 'maximum the hormone',
        'genre' => 'nu metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Ho',
        'recorded_date' => '2001',
        'release_date'  => '2001',
        'number_of_tracks'  => 6,
        'label' => 'sky records',
        'producer' => 'maximum the hormone',
        'genre' => 'hardcore punk',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Mimi Kajiru',
        'recorded_date' => '2002',
        'release_date'  => '2002',
        'number_of_tracks'  => 10,
        'label' => 'mimikajiru',
        'producer' => 'maximum the hormone',
        'genre' => 'heavy metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Kusoban',
        'recorded_date' => '2004',
        'release_date'  => '2004',
        'number_of_tracks'  => 8,
        'label' => 'mimikajiru',
        'producer' => 'maximum the hormone',
        'genre' => 'funk metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Rokkinpo Goroshi',
        'recorded_date' => '2005',
        'release_date'  => '2005',
        'number_of_tracks'  => 5,
        'label' => 'VAP',
        'producer' => 'maximum the hormone',
        'genre' => 'heavy metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Debu vs Debu',
        'recorded_date' => '2005',
        'release_date'  => '2005',
        'number_of_tracks'  => 20,
        'label' => 'VAP',
        'producer' => 'maximum the hormone',
        'genre' => 'metalcore',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Buiikikaesu',
        'recorded_date' => '2007',
        'release_date'  => '2007',
        'number_of_tracks'  => 14,
        'label' => 'VAP',
        'producer' => 'maximum the hormone',
        'genre' => 'metalcore',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 7,
        'name'    => 'Yoshu Fukushu',
        'recorded_date' => '2013',
        'release_date'  => '2013',
        'number_of_tracks'  => 16,
        'label' => 'VAP',
        'producer' => 'maximum the hormone',
        'genre' => 'groove metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => 'Bon Jovi',
        'recorded_date' => '1982',
        'release_date'  => '1983',
        'number_of_tracks'  => 9,
        'label' => 'mercury records',
        'producer' => 'Tony Bongiovi, Lance Quinn',
        'genre' => 'hard rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => '7800° fahrenheit',
        'recorded_date' => '1985',
        'release_date'  => '1985',
        'number_of_tracks'  => 10,
        'label' => 'mercury & vertigo records',
        'producer' => 'Lance Quinn',
        'genre' => 'glam metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => 'Slippery When Wet',
        'recorded_date' => '1986',
        'release_date'  => '1986',
        'number_of_tracks'  => 10,
        'label' => 'mercury & vertigo records',
        'producer' => 'Bruce Fairbairn',
        'genre' => 'hard rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => 'New Jersey',
        'recorded_date' => '1988',
        'release_date'  => '1988',
        'number_of_tracks'  => 12,
        'label' => 'polygram records',
        'producer' => 'Bruce Fairbairn',
        'genre' => 'heavy metal',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => 'Blaze of Glory',
        'recorded_date' => '1990',
        'release_date'  => '1990',
        'number_of_tracks'  => 11,
        'label' => 'mercury records',
        'producer' => 'Jon Bon Jovi, Danny Kortchmar',
        'genre' => 'hard rock',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('albums')->insert
      ([
        'band_id' => 10,
        'name'    => 'Keep the Faith',
        'recorded_date' => '1992',
        'release_date'  => '1992',
        'number_of_tracks'  => 12,
        'label' => 'mercury records',
        'producer' => 'Bob Rock',
        'genre' => 'rock music',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);
    }
}
