<?php

use Illuminate\Database\Seeder;

class BandsTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('bands')->insert([
      	'name' => 'Bloodgood',
      	'start_date' => '1984',
      	'website' => 'http://www.bloodgoodband.com',
      	'still_active' => true,
      	'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Avenged Sevenfold',
        'start_date' => '1999',
        'website' => 'http://www.avengedsevenfold.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'A9',
        'start_date' => '2004',
        'website' => 'http://www.a9-project.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Westlife',
        'start_date' => '1998',
        'website' => 'http://westlife.com',
        'still_active' => false,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'One Direction',
        'start_date' => '2010',
        'website' => 'http://www.onedirectionmusic.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'One Ok Rock',
        'start_date' => '2005',
        'website' => 'http://www.oneokrock.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Maximum The Hormone',
        'start_date' => '1999',
        'website' => 'http://www.maximumthehormone.jp',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Man With a Missions',
        'start_date' => '2010',
        'website' => 'http://www.mwamjapan.info',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Uverworld',
        'start_date' => '2000',
        'website' => 'http://www.uverworld.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      DB::table('bands')->insert([
        'name' => 'Bon Jovi',
        'start_date' => '1987',
        'website' => 'http://www.bonjovi.com',
        'still_active' => true,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);
    }
}
