<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Band as Band;
use Session;

class BandController extends Controller
{
  public function index(Request $request)
  {
  	$bands = Band::all();
  	// apply sorting, if necessary
  	if($request->input('sort') !== '')
  	{
  	  $sort = $request->input('sort');
  	  $sortdirection = $request->sortdirection;
  	  $this->sort($bands, $sort, $sortdirection);
  	  // this has to come afterward so that toggling works
  	  $sortdirection = $request->sortdirection === 'asc' ? 'desc' : 'asc'; 
  	}

  	return view('bands.index', ['bands' => $bands, 'sort' => $sort, 'sortdirection' => $sortdirection]);
  }

  public function create()
  {
  	return view('bands.create');
  }

  public function store(Request $request)
  {
  	// validate the data
  	$this->validate($request, [
  	  'name' 	   	=> 'required|unique:bands, name|max:255|string',
  	  'start_date' 	=> 'nullable|string',
  	  'website'	   	=> 'nullable|url',
  	  'still_active'=> 'boolean'
  	]);

    // extract and store data
    $band = new Band();
    $band->name = $request->name;
    $band->start_date = $request->start_date;
    $band->website = $request->website;
    $band->still_active = (bool)$request->still_active;
    $band->save();

    // flash message
    Session::flash('success', "The band $band->name was successfully saved!");

    // return show route
    return redirect()->route('bands.show', ['band' => $band]);
  }

  public function show($id)
  {
  	$band = Band::find($id);
  	return view('bands.show', ['band' => $band]);
  }

  public function edit($id)
  {
  	$band = Band::find($id);
  	return view('bands.edit', ['band' => $band]);
  }

  public function update(Request $request, $id)
  {
  	// validate the data
  	$band = Band::find($id);
  	if($band->name === $request->name)
  	{
  	  $this->validate($request, [
  	  	  'start_date' 	=> 'sometimes|string',
  	  	  'website'    	=> '',
  	  	  'still_active'=> 'sometimes|boolean'
  	  ]);
  	} else {
  	  $this->validate($request, [
  	  	'name' 			=> 'required|max:255|alpha_dash',
  	  	'start_date'	=> 'nullable|string',
  	  	'website' 		=> 'nullable|url',
  	  	'still_active'  => 'boolean'
  	  ]);
  	}

  	// extract and store data
  	$band = Band::find($id);
  	$band->name = $request->name;
  	$band->start_date = $request->start_date;
  	$band->website = $request->website;
  	$band->still_active = $request->still_active;
  	$band->save();

  	// flash message
  	Session::flash('success', "The band $band->name was successfully saved!");

  	// return show route
  	return redirect()->route('bands.show', ['band' => $band]);
  }

  public function destroy($id)
  {
  	$band = Band::find($id);
  	$albums = $band->albums;
  	if($albums->count() !== 0)
  	{
  	  foreach($albums as $album)
  	  {
  	  	$album->delete();
  	  }
  	}
  	$band->delete();
  	Session::flash('success', "$band->name and their albums were successfully deleted.");
  	return redirect()->route('bands.index');
  }
}
