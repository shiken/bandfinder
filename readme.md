## About Band Finder Project

This application just showed data about music bands 

## Install This Project

Read this one https://stackoverflow.com/questions/25610958/installing-laravel-on-existing-project
surely can help you.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Screenshots

![Alt text](https://bytebucket.org/shiken/bandfinder/raw/6ca59b26c07b7f1b49b83f8c245991a52d9ba1f8/screenshot.JPG)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
